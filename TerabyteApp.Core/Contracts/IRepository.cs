﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerabyteApp.Core.Models;

namespace TerabyteApp.Core.Contracts
{
    public interface IRepository<T> where T : BaseEntity
    {
        IQueryable<T> GetCollection();
        void InsertEntity(T t);
        void DeleteEntity(string id);
        void CommitChanges();
        T GetEntity(string id);
        void UpdateEntity(T t);
    }
}
