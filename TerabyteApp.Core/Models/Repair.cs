﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerabyteApp.Core.Models
{
    public class Repair : BaseEntity
    {
        public string DeviceId { get; set; }
        public string CustomerId { get; set; }

        public string RepairDate { get; set; }
        public string RepairType { get; set; }
        public string RepairDecscription { get; set; }
        public float RepairTotalPrice { get; set; }
    }
}
