﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerabyteApp.Core.Models
{
    public abstract class BaseEntity
    {
        public string Id { get; set; }
        public string CreationTime;

        public BaseEntity()
        {
            Id = Guid.NewGuid().ToString();
            CreationTime = DateTime.Now.ToString();
        }
    }
}
