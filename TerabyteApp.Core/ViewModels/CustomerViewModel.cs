﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerabyteApp.Core.Models;

namespace TerabyteApp.Core.ViewModels
{
    public class CustomerViewModel
    {
        public Customer customer { get; set; }
        public List<Device> devices { get; set; }
        public List<Repair> repairs { get; set; }

        public CustomerViewModel()
        {
            customer = new Customer();
            devices = new List<Device>();
            repairs = new List<Repair>();
        }
    }
}
