﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerabyteApp.Core.ViewModels
{
    public enum RepairType
    {
        SOFTWARE,
        HARDWARE,
        OTHER
    }

    public class RepairViewModel
    {
        public RepairType repairType { get; set; }
        public string repairDescription { get; set; }
        public string customerName { get; set; }
        public string deviceName { get; set; }
    }
}
