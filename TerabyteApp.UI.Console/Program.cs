﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerabyteApp.Core.Models;
using TerabyteApp.DataAccess.SQL;

namespace TerabyteApp.UI.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            TerabyteConsoleApp consoleApp = new TerabyteConsoleApp();

            consoleApp.Initialise();

            while(consoleApp.GetIsrunning())
            {
                consoleApp.Update();
            }

            System.Console.WriteLine("Program Terminated. ");
            
            //Customer customer = consoleApp.CreateCustomer();
            //consoleApp.AddCustomerToDataBase(customer);


        }


    }
}
