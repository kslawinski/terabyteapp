﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerabyteApp.Core.Models;
using TerabyteApp.DataAccess.SQL;

namespace TerabyteApp.UI.Console
{
    public class TerabyteConsoleApp
    {
        private DataContext context = new DataContext();

        private bool bShouldQuit = true;
        private bool isRunning = false;

        int selectionIndex = 0;

        public bool GetIsrunning()
        {
            return isRunning;
        }

        public void Initialise()
        {
            bShouldQuit = false;
            isRunning = true;
        }

        public void Terminate()
        {
            bShouldQuit = true;
            isRunning = false;
        }

        public void Update()
        {
            System.Console.WriteLine("Press 1 to print customer List, Press 2 to add new customer, Press 0 to Terminate program \n");

            string input = System.Console.ReadLine();

            switch (input)
            {
                case "1":
                    {
                        System.Console.WriteLine(PrintCustomerList());
                        string outPutString = "";
                        outPutString += "++++++++++++++++++++++++++++++++++++ \n\n";
                        outPutString += "Make selection for Customer Details or type 0 to go back \n";

                        int selectionInput = Convert.ToInt32(System.Console.ReadLine());

                        if (selectionInput > -1)
                        {
                            SQLRepository<Customer> customerRepo = new SQLRepository<Customer>(context);
                            List<Customer> customerList = customerRepo.GetCollection().ToList();
                            Customer customer1 = customerList[selectionInput];
                            PrintCustomerDetails(customer1);
                        }
                        break;
                    }
                case "2":
                    Customer customer = CreateCustomer();
                    AddCustomerToDataBase(customer);
                    break;
                case "0":
                    Terminate();
                    break;
            }
        }

        public string PrintCustomerList()
        {
            string outPutString = "";
            selectionIndex = 0;
            SQLRepository<Customer> customerRepo = new SQLRepository<Customer>(context);
            List<Customer> customerList = customerRepo.GetCollection().ToList();

            outPutString += "++++++++++++ Customers +++++++++++++ \n\n";

            foreach (Customer customer in customerList)
            {
                outPutString += "----------------------------------- \n";
                outPutString += "Name: " + customer.Name + "\n";
                outPutString += "Surname: " + customer.Surname + "\n";
                outPutString += "Telephone: " + customer.Telephone + "\n";
                outPutString += "Customer Details: " + selectionIndex + "\n";
                outPutString += "----------------------------------- \n\n";
                selectionIndex++;
            }

            return outPutString;
        }

        public Customer CreateCustomer()
        {
            Customer customer = new Customer();

            System.Console.WriteLine("Name: ");
            customer.Name =  System.Console.ReadLine();
            System.Console.WriteLine("Surname: ");
            customer.Surname = System.Console.ReadLine();
            System.Console.WriteLine("Telephone: ");
            customer.Telephone = System.Console.ReadLine();

            return customer;
        }

        public void AddCustomerToDataBase(Customer customer)
        {
            SQLRepository<Customer> customerRepo = new SQLRepository<Customer>(context);

            customerRepo.InsertEntity(customer);
            customerRepo.CommitChanges();
        }

        public void PrintCustomerDetails(Customer customer)
        {
            string outPutString = "";
            outPutString += "----------------------------------- \n";
            outPutString += "Name: " + customer.Name + "\n";
            outPutString += "Surname: " + customer.Surname + "\n";
            outPutString += "Telephone: " + customer.Telephone + "\n";
            outPutString += "----------------------------------- \n\n";
            System.Console.WriteLine(outPutString);
        }

        public void EditCustomer(Customer customer)
        {
            
        }
    }
}
