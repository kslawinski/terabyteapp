﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerabyteApp.Core.Contracts;
using TerabyteApp.Core.Models;

namespace TerabyteApp.DataAccess.InMemory
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        public void CommitChanges()
        {
            throw new NotImplementedException();
        }

        public void DeleteEntity(string id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> GetCollection()
        {
            throw new NotImplementedException();
        }

        public T GetEntity(string id)
        {
            throw new NotImplementedException();
        }

        public void InsertEntity(T t)
        {
            throw new NotImplementedException();
        }

        public void UpdateEntity(T t)
        {
            throw new NotImplementedException();
        }
    }
}
