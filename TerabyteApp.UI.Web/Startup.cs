﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TerabyteApp.UI.Web.Startup))]
namespace TerabyteApp.UI.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
