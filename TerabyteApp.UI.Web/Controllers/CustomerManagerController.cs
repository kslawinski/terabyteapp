﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TerabyteApp.Core.Models;
using TerabyteApp.Core.ViewModels;
using TerabyteApp.DataAccess.SQL;

namespace TerabyteApp.UI.Web.Controllers
{
    public class CustomerManagerController : Controller
    {
        DataContext context = new DataContext();
        SQLRepository<Customer> customerRepo;

        public CustomerManagerController()
        {
            customerRepo = new SQLRepository<Customer>(context);
        }

        // GET: CustomerManager
        public ActionResult Index()
        {
            List<Customer> customers = customerRepo.GetCollection().ToList();

            return View(customers);
        }

        public ActionResult Create()
        {
            Customer customer = new Customer();
            return View(customer);
        }

        [HttpPost]
        public ActionResult Create(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return View(customer);
            }
            else
            {
                customerRepo.InsertEntity(customer);
                customerRepo.CommitChanges();

                return RedirectToAction("Index");
            }
        }

        public ActionResult Details(string id)
        {
            List<Device> customerDeviceList = new List<Device>();
            List<Repair> customerRepairList = new List<Repair>();

            Customer customer = customerRepo.GetEntity(id);
            CustomerViewModel customerView = new CustomerViewModel();

            SQLRepository<Repair> repairRepo = new SQLRepository<Repair>(context);
            SQLRepository<Device> deviceRepo = new SQLRepository<Device>(context);

            List<Device> deviceList = deviceRepo.GetCollection().ToList();
            customerDeviceList = deviceList.FindAll(c => c.OwnerID == id);

            List<Repair> repairsList = repairRepo.GetCollection().ToList();
            customerRepairList = repairsList.FindAll(c => c.CustomerId == id);

            customerView.customer = customer;
            customerView.devices = customerDeviceList;
            customerView.repairs = customerRepairList;

            return View(customerView);
        }
    }
}