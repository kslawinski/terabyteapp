namespace TerabyteApp.DataAccess.SQL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Surname = c.String(),
                        Telephone = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Devices",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        OwnerID = c.String(),
                        Make = c.String(),
                        Model = c.String(),
                        SnNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Repairs",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        DeviceId = c.String(),
                        CustomerId = c.String(),
                        RepairDate = c.String(),
                        RepairType = c.String(),
                        RepairDecscription = c.String(),
                        RepairTotalPrice = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Repairs");
            DropTable("dbo.Devices");
            DropTable("dbo.Customers");
        }
    }
}
