﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerabyteApp.Core.Contracts;
using TerabyteApp.Core.Models;

namespace TerabyteApp.DataAccess.SQL
{
    public class SQLRepository<T> : IRepository<T> where T : BaseEntity
    {
        internal DataContext context;
        internal DbSet<T> entitydbSet;

        public SQLRepository(DataContext context)
        {
            this.context = context;
            this.entitydbSet = context.Set<T>();
        }

        public void CommitChanges()
        {
            context.SaveChanges();
        }

        public void DeleteEntity(string id)
        {
            var entity = entitydbSet.Find(id);
            if (entity != null)
                if (context.Entry(entity).State == EntityState.Detached)
                {
                    entitydbSet.Attach(entity);
                }
                else
                {
                    entitydbSet.Remove(entity);
                }
        }

        public IQueryable<T> GetCollection()
        {
             return entitydbSet;
        }

        public T GetEntity(string id)
        {
            return entitydbSet.Find(id);
        }

        public void InsertEntity(T t)
        {
            entitydbSet.Add(t);
        }

        public void UpdateEntity(T t)
        {
            entitydbSet.Attach(t);
            context.Entry(t).State = EntityState.Modified; // set the entry as being modified now
        }
    }
}
